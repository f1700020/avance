@extends('layouts.app')
@section('content')

<!-- - - - - - - - - - - - - - Revolution Slider - - - - - - - - - - - - - - - - -->
<div class="rev-slider-wrapper">
    <div id="rev-slider" class="rev-slider" data-version="5.0">

        <ul>
            <li data-transition="fade" class="align-center">
                <img src="public/images/tablet.jpg" class="rev-slidebg" alt="">
            </li>
            <li data-transition="fade" class="align-center">
                <img src="public/images/kids.jpg" class="rev-slidebg" alt="">
            </li>
            <li data-transition="fade" class="align-center">
                <img src="public/images/workshop.jpg" class="rev-slidebg" alt="">
            </li>
        </ul>

    </div>
</div>
<!-- - - - - - - - - - - - - - End of Slider - - - - - - - - - - - - - - - - -->

<!-- page-section -->
<!-- page-section -->
<!-- page-section -->
<div class="page-section">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
              <div class="title-holder" style="text-align: center;">
                  <h4 class="section-title">Optimizamos la plataforma educativas para mejorar los procesos de capacitación online de su personal.</h4>
              </div>
            </div>
            <div class="col-md-12" style="margin-top:30px;">
              <div class="title-holder" style="text-align: center;">
                  <h4 class="section-title">Configuramos plataformas LMS (Sistema de Gestión de Aprendizaje) para el E-Learning empresarial</h4>
              </div>
            </div>
        </div>
    </div>
</div>
<!--End page-section -->
<div class="holder-bg type-3 parallax-section" data-bg="public/images/prueba1.jpg" id="contact">

    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <h2 class="section-title" style="color: black;text-align: center;">Beneficios de las Plataforma LMS</h2>
                <div class="accordion style-2">

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title active"  style="background-color: rgba(19, 20, 54, 0.678)">Tiempos cortos de capacitación</h6>
                        <div class="a-content">

                            <p>
                            Capacita en tiempo cortos y a distancia a todos tus empleados y/o usuarios sin que se
                            expongan a contagios. Aprovecha la disponibilidad de los recursos de capacitación 24/7.
                            </p>
                        </div>
                    </div>

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title " style="background-color: rgba(10, 10, 10, 0.514)">Mejora la relación Costo - Beneficio</h6>
                        <div class="a-content">

                            <p>
                                El E-learning reduce los costos de capacitación al prescindir de gastos de renta de
                                espacios y de viáticos para el desplazamiento del personal.
                            </p>
                        </div>
                    </div>

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title" style="background-color: rgba(10, 10, 10, 0.514)">Crea Centros de Entrenamiento</h6>
                        <div class="a-content">
                            <p>
                                Crea cursos e inscribe a sus participantes de manera fácil y cómoda. Diseña Ciclos, niveles, grados y grupos.
                            </p>
                            <p>
                                Supervisa las actividades y recursos empleados para la capacitación. Utiliza Videoconferencias para webinars y clases magisteriales.
                            </p>
                            <p>
                                Verifica las evidencias entregadas por el personal que se capacita, así como identifica la evaluación asignada.
                            </p>
                            <p>
                                Administra el Alta y modificación de los perfiles de usuarios de tu colegio. Asígnalos a grupos y materias.
                            </p>
                            <p>
                                Crea Sistemas de aprendizaje basado en competencias, emplea insignias y expide certificados.
                            </p>
                            <p>
                                Administra los roles de Profesor o alumnos y monitorea sus avances.
                            </p>
                            <p>
                                Interactúa con los alumnos mediante Chats, Foros, Encuestas y Retroalimentación entre otras actividades.
                            </p>
                        </div>
                    </div>

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title" style="background-color: rgba(10, 10, 10, 0.514)">Creación de historial de capacitación</h6>
                        <div class="a-content">
                            <p>
                                Administra el historial de cursos de tu personal. Conoce su desempeño en los cursos que se inscribió.
                            </p>

                        </div>
                    </div>

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title" style="background-color: rgba(10, 10, 10, 0.514)">Lo que se mide se puede mejorar</h6>
                        <div class="a-content">
                            <p>
                                Recopilación y análisis de datos sobre las interacciones de los alumnos en la plataforma Moodle para fortalecer el aprendizaje activo. Usa los datos de la analítica de aprendizaje.
                            </p>

                        </div>
                    </div>

                    <!--accordion item-->
                    <div class="accordion-item">
                        <h6 class="a-title" style="background-color: rgba(10, 10, 10, 0.514)">Biblioteca de cursos y/o capacitaciones</h6>
                        <div class="a-content">
                            <p>
                                Los LMS permiten, mediante el Almacenamiento en la nube, la disponibilidad de tus cursos con lo que se mejora la gestión del conocimiento creado por tu empresa.
                            </p>

                        </div>
                    </div>



                </div>

            </div>






        </div>
    </div>
</div>
<br>
<!-- page-section -->
<div class="page-section">

    <div class="container">

        <div class="content-element4">

            <div class="row">

                <main id="main" class="col-md-8">

                    <!-- content element -->
                    <div class="content-element2">
                        <div class="content-element2">

                            <div class="content-element3">
                                <div class="section-title" style="color: black;text-align: center;">Nuestro servicio de Optimización y personalización de plataforma Moodle incluye</div>
                                    <ul class="custom-list type-2 style-3">

                                       <li>Configuración y Optimización de la plataforma Moodle. </li>
                                       <li>Implementación de sus elementos de indentidad corporativa.</li>
                                       <li>Capacitación sobre uso de la plataforma.</li>
                                       <li>Costo de Implementación: MXN 5,000 Se paga en una sola ocasión.</li>

                                    </ul>

                            </div>

                        </div>

                    </div>
                </main>
            </div>

        </div>

    </div>

</div>
<!--End page-section -->


<br><br>
<!-- Tabla de paquetes -->
<div class="page-section-bg align-center">

    <div class="container">

        <h2 class="section-title type2">Paquetes</h2>

        <!-- welcome area -->
        <div class="welcome-section blog-type fx-col-4">

            <!-- welcome element -->
            <div class="welcome-col">

                <div class="welcome-item">

                    <div class="welcome-inner">

                        <div class="welcome-img">
                            <img src="images/girl-3718526_641.jpg" alt="">
                            <time class="entry-date" datetime="2016-08-20">
                                <span>20</span>EMP
                            </time>
                        </div>

                        <div class="welcome-content">

                            <svg class="bigHalfCircle" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
                                <path d="M0 100 C40 0 60 0 100 100 Z"></path>
                            </svg>

                            <div class="entry">

                                <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->

                                <div class="entry-body">

                                    <h5 class="entry-title"><a href="#">EMPRENDE</a></h5>
                                    <p class="section-title">Costo Mensual <br> MXN 3,000</p>
                                    <p class="section-title">Usuarios <br> Hasta 100</p>
                                    <p class="section-title">Hosting <br> Incluye</p>
                                    <p class="section-title">Soporte Técnico <br> Incluye</p>


                                </div>

                                <!-- - - - - - - - - - - - - - End of Entry body - - - - - - - - - - - - - - - - -->

                                <button class="btn btn-outling my-2 my-sm-0">enviar</button>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- welcome element -->
            <div class="welcome-col">

                <div class="welcome-item">

                    <div class="welcome-inner">

                        <div class="welcome-img">
                            <img src="images/360x220_img5.jpg" alt="">
                            <time class="entry-date" datetime="2016-08-17">
                                <span>17</span>REG
                            </time>
                        </div>

                        <div class="welcome-content">

                            <svg class="bigHalfCircle" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
                                <path d="M0 100 C40 0 60 0 100 100 Z"></path>
                            </svg>

                            <div class="entry">

                                <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->

                                <div class="entry-body">

                                    <h5 class="entry-title"><a href="#">REGIONAL</a></h5>
                                    <p class="section-title">Costo Mensual <br> MXN 4,200</p>
                                    <p class="section-title">Usuarios <br> 101 - 200</p>
                                    <p class="section-title">Hosting <br> Incluye</p>
                                    <p class="section-title">Soporte Técnico <br> Incluye</p>

                                </div>

                                <!-- - - - - - - - - - - - - - End of Entry body - - - - - - - - - - - - - - - - -->

                                <button class="btn btn-outling my-2 my-sm-0">enviar</button>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <!-- welcome element -->
            <div class="welcome-col">

                <div class="welcome-item">

                    <div class="welcome-inner">

                        <div class="welcome-img">
                            <img src="images/360x220_img6.jpg" alt="">
                            <time class="entry-date" datetime="2016-08-10">
                                <span>10</span>COR
                            </time>
                        </div>

                        <div class="welcome-content">

                            <svg class="bigHalfCircle" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
                                <path d="M0 100 C40 0 60 0 100 100 Z"></path>
                            </svg>

                            <div class="entry">

                                <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->

                                <div class="entry-body">

                                    <h5 class="entry-title"><a href="#">CORPORATIVO</a></h5>
                                    <p class="section-title">Costo Mensual <br> MXN 5,670</p>
                                    <p class="section-title">Usuarios <br>201 - 400</p>
                                    <p class="section-title">Hosting <br> Incluye</p>
                                    <p class="section-title">Soporte Técnico <br> Incluye</p>


                                </div>

                                <!-- - - - - - - - - - - - - - End of Entry body - - - - - - - - - - - - - - - - -->



                                <button class="btn btn-outling my-2 my-sm-0">enviar</button>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- welcome element -->
            <div class="welcome-col">

                <div class="welcome-item">

                    <div class="welcome-inner">

                        <div class="welcome-img">
                            <img src="images/360x220_img6.jpg" alt="">
                            <time class="entry-date" datetime="2016-08-10">
                                <span>10</span>GLO
                            </time>
                        </div>

                        <div class="welcome-content">

                            <svg class="bigHalfCircle" xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none">
                                <path d="M0 100 C40 0 60 0 100 100 Z"></path>
                            </svg>

                            <div class="entry">

                                <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->

                                <div class="entry-body">

                                    <h5 class="entry-title"><a href="#">GLOBAL</a></h5>
                                    <p class="section-title">Costo Mensual <br> MXN 7,370</p>
                                    <p class="section-title">Usuarios <br> 401 - 800</p>
                                    <p class="section-title">Hosting <br> Incluye</p>
                                    <p class="section-title">Soporte Técnico <br> Incluye</p>






                                </div>

                                <!-- - - - - - - - - - - - - - End of Entry body - - - - - - - - - - - - - - - - -->
                                <button class="btn btn-outling my-2 my-sm-0">enviar</button>



                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <a href="#" class="btn type-2 btn-style-6">More News</a>

    </div>

</div>
<!-- Tabla de paquetes -->


<!-- page-section -->
<div class="page-section">

    <div class="container">
        <div class="row">
            <div class="col-md-6">

                 <img src="public/images/notebook.webp" alt="">

                    </div>
                        <div class="col-md-6">

                            <div class="title-holder">

                                <h2 class="section-title" style="color: black">El servicio se puede cancelar en cualquier momento. No hay plazos forzosos. </h2>
                                <div class="text-wrap">
                                    <ul class="news-list" style="list-style: upper-roman;">
                                        <li>Costo mensual no incluye IVA.</li>
                                        <li>Se expide factura una vez reflejado su pago en nuestro sistema.</li>


                                    </ul>
                                </div>


                            </div>

                        </div>

        </div>
    </div>
</div>
<br>


<!-- page-section -->
<div class="jumbotron">
                  <div class="page-section">

                    <div class="container">

                            <div >

                                <div class="title-holder">

                                    <h2 class="section-title">Agrega tu presencia web, contrata tu Landing Page junto con tu Plataforma LMS</h2>
                                    <div class="text-wrap">
                                        <h4>Por MXN 3,000 obtienes</h4>
                                        <ul class="news-list" style="list-style: upper-roman;">
                                            <li>Diseño personalizado de tu presencia web</li>
                                            <li>Dominio y Hosting gratis para la Landing Page. Al año siguiente se paga
                                                exclusivamente la renovación del dominio. </li>
                                            <li>Estrategia SEO para tu presencia web</li>
                                            <li>Implementación de código de seguimiento Google Analytics</li>
                                            <li>Reporte Google Analytics gratis los dos primeros meses.</li>
                                            <li>IVA incluído</li>


                                        </ul>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>

</div>
<br>

<!-- Contacto-->


<section class="form_wrap">

    <section class="cantact_info">
        <section class="info_title">
            <span class="fa fa-user-circle"></span>
            <h2>INFORMACION<br>DE CONTACTO</h2>
        </section>
        <section class="info_items">
            <p><span class="fa fa-envelope"></span>  contacto@siacademico.com </p>
            <p><span class="fa fa-mobile"></span> Llamanos:
                81 3280 6645
                </p>
        </section>
    </section>

    <form action="" class="form_contact">
        <h2>Sistemas de Gestión de Aprendizaje</h2>
        <div class="user_info">
            <label for="names">Nombre:</label>
            <input type="text" id="names">

            <label for="phone">Número celular de contacto</label>
            <input type="tel" id="phone">

            <label for="email">Email</label>
            <input type="text" id="email">

            <label for="mensaje">Paqute que requiere:</label>
            <select type="text" id="mensaje">
                <option>Emprende</option>
                <option>Regional</option>
                <option>Corporativo</option>
                <option>Global</option>

              </select>


        </div>
        <div class="container text-right" style="max-width:100%;display:flex;justify-content:flex-end;">
            <button class="btn btn-info mb-5">Enviar</button>
        </div>
    </form>

</section>
<!-- Contacto -->


<!-- page-section -->


</body>
  <br><br>

@stop
