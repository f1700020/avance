@extends('layouts.app')
@section('content')
            <!-- - - - - - - - - - - - - - Revolution Slider - - - - - - - - - - - - - - - - -->

            <div class="rev-slider-wrapper">

                <div id="rev-slider" class="rev-slider" data-version="5.0">

                    <ul>

                        <li data-transition="fade" class="align-center">

                            <img src="public/images/girl-3041375_1920.jpg" class="rev-slidebg" alt="">

                            <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme scaption-white-text rs-parallaxlevel-1" data-x="center" data-y="top" data-voffset="180" data-whitespace="nowrap" data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-responsive_offset="on" data-elementdelay="0.05">
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme scaption-white-large rs-parallaxlevel-2" data-x="center" data-y="top" data-voffset="225" data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>Mejores herramientas
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Layer 3 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme rs-parallaxlevel-3" data-x="center" data-y="top" data-voffset="420" data-whitespace="nowrap" data-frames='[{"delay":150,"speed":2500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'>
                                <a href="" style="color: black;font-weight: 800;" class="btn btn-big type-2 btn-style-7">Leer mas</a>
                                <a href="#contact" class="btn btn-big">Cotizar</a>
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 3 - - - - - - - - - - - - - - - - -->

                        </li>

                        <li data-transition="fade" class="align-center">

                            <img src="public/images/Portada2-08-08.jpg" class="rev-slidebg" alt="">

                            <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme scaption-white-text rs-parallaxlevel-1" data-x="center" data-y="top" data-voffset="180" data-whitespace="nowrap" data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-responsive_offset="on" data-elementdelay="0.05">Mejores herramientas
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme scaption-white-large rs-parallaxlevel-2" data-x="center" data-y="top" data-voffset="225" data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'><span style="color:#fff;">SIA Instala, optimiza y <br> personaliza la plataforma </span><span style="color:#f05a21">Moodle</span>
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Layer 3 - - - - - - - - - - - - - - - - -->

                            <div class=" tp-caption tp-resizeme rs-parallaxlevel-3" data-x="center" data-y="top" data-voffset="420" data-whitespace="nowrap" data-frames='[{"delay":150,"speed":2500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'>
                                <a href="" style="color: #fff;font-weight: 800;" class="btn btn-big type-2 btn-style-7">Leer mas</a>
                                <a href="#contact" class="btn btn-big">Cotizar</a>
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 3 - - - - - - - - - - - - - - - - -->

                        </li>

                        <li data-transition="fade" class="align-center">

                            <img src="public/images/17923.jpg" class="rev-slidebg" alt="">

                            <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme scaption-white-text rs-parallaxlevel-1" data-x="center" data-y="top" data-voffset="180" data-whitespace="nowrap" data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-responsive_offset="on" data-elementdelay="0.05">Mejor colegio
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme scaption-white-large rs-parallaxlevel-2" data-x="center" data-y="top" data-voffset="225" data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>Tu colegio aproveche al <br>máximo los beneficios de Moodle
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Layer 3 - - - - - - - - - - - - - - - - -->

                            <div class="tp-caption tp-resizeme rs-parallaxlevel-3" data-x="center" data-y="top" data-voffset="420" data-whitespace="nowrap" data-frames='[{"delay":150,"speed":2500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'>
                                <a href="" style="color: black;font-weight: 800;" class="btn btn-big type-2 btn-style-7">Leer mas</a>
                                <a href="#contact" class="btn btn-big">Cotizar</a>
                            </div>

                            <!-- - - - - - - - - - - - - - End of Layer 3 - - - - - - - - - - - - - - - - -->

                        </li>

                    </ul>

                </div>
            </div>

            <!-- - - - - - - - - - - - - - End of Slider - - - - - - - - - - - - - - - - -->

            <!-- page-section -->
            <div class="page-section">

                <div class="container">
                    <div class="row">
                        <div class="col-md-6">

                            <img src="public/images/classroom-2787754_640.jpg" alt="">

                        </div>
                        <div class="col-md-6">

                            <div class="title-holder">

                                <h2 class="section-title">En cualquier lugar <br> y en cualquier momento</h2>
                                <div class="text-wrap">
                                    <ul class="news-list" style="list-style: upper-roman;">
                                        <li>Almacenamiento en la nube</li>
                                        <li>Disponibilidad 24/7</li>
                                        <li>Accede desde cualquier dispositivo móvil / Tableta / Ipad /Laptop o PC</li>
                                        <li>Y desde cualquier sistema: iOS - Android</li>

                                    </ul>
                                </div>
                                <!-- <a href="#" class="info-btn">More About Us</a> -->

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- page-section -->
            <div class="section-with-carousel with-bg parallax-section" data-bg="public/images/students-99506_1920.jpg">

                <div class="comment-section align-center">

                    <div class="container">

                        <h2 class="section-title">Con la consultoría de SIA, usted podrá:</h2>

                        <!-- - - - - - - - - - - - - Owl-Carousel - - - - - - - - - - - - - - - -->

                        <div class="carousel-type-1">

                            <div class="owl-carousel testimonial type-2" data-max-items="3" data-item-margin="30" data-autoplay="true">

                                <!-- Slide -->
                                <div class="item-carousel">
                                    <!-- Carousel Item -->

                                    <div class="comment-item">

                                        <div class="comment-holder">

                                            <blockquote>
                                                <p>Transformar a su institución con tecnología de vanguardia</p>
                                            </blockquote>

                                        </div>

                                        <!-- bottom-separator -->
                                        <div class="bottom-separator"></div>

                                    </div>

                                    <!-- <div class="author-box">

                                        <a href="" class="avatar">
                                            <img src="public/images/100x100_author1.jpg" alt="">
                                        </a>

                                    </div> -->

                                    <!-- /Carousel Item -->
                                </div>
                                <!-- /Slide -->

                                <!-- Slide -->
                                <div class="item-carousel">
                                    <!-- Carousel Item -->

                                    <div class="comment-item">

                                        <div class="comment-holder">

                                            <blockquote>
                                                <p>Lograr una institución innovadora con el uso de Moodle.</p>
                                            </blockquote>

                                        </div>

                                        <!-- bottom-separator -->
                                        <div class="bottom-separator"></div>

                                    </div>

                                    <!-- <div class="author-box">

                                        <a href="#" class="avatar">
                                            <img src="public/images/100x100_author2.jpg" alt="">
                                        </a>

                                    </div> -->

                                    <!-- /Carousel Item -->
                                </div>
                                <!-- /Slide -->

                                <!-- Slide -->
                                <div class="item-carousel">
                                    <!-- Carousel Item -->

                                    <div class="comment-item">

                                        <div class="comment-holder">

                                            <blockquote>
                                                <p>Alcanzar un mayor número de matrículas.</p>
                                            </blockquote>

                                        </div>

                                        <!-- bottom-separator -->
                                        <div class="bottom-separator"></div>

                                    </div>
                                    <!--
                                    <div class="author-box">

                                        <a href="#" class="avatar">
                                            <img src="public/images/100x100_author3.jpg" alt="">
                                        </a>

                                    </div> -->

                                    <!-- /Carousel Item -->
                                </div>
                                <!-- /Slide -->

                                <!-- Slide -->
                                <div class="item-carousel">
                                    <!-- Carousel Item -->

                                    <div class="comment-item">

                                        <div class="comment-holder">

                                            <blockquote>
                                                <p>Obtener mejores resultados académicos.</p>
                                            </blockquote>

                                        </div>

                                        <!-- bottom-separator -->
                                        <div class="bottom-separator"></div>

                                    </div>

                                    <!-- <div class="author-box">

                                        <a href="#" class="avatar">
                                            <img src="public/images/100x100_author1.jpg" alt="">
                                        </a>
                                    </div> -->

                                    <!-- /Carousel Item -->
                                </div>
                                <!-- /Slide -->
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            <!-- page-section -->
            <div class="page-section">

                <div class="container">

                    <div class="align-center">

                        <h2 class="section-title">Ventajas de <span style="color: #f05a21;"> Moodle</span></h2>

                        <div class="icons-box type-2 fx-col-4">

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/video.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Streaming</a>
                                        </h4>
                                        <p>
                                            Transmite en vivo tus clases, graba y comparte.
                                        </p>

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/pc.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Recursos</a>
                                        </h4>
                                        <p>
                                            Comparte presentaciones, enlaces URL, carpetas y demás recursos con tus alumnos.
                                        </p>

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/businesswoman.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Actividades</a>
                                        </h4>
                                        <p>
                                            Asigna actividades como tareas, exámenes, cuestionarios, juegos, etc.
                                        </p>

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/exam.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Calificaciones</a>
                                        </h4>
                                        <p>
                                            Automatiza tus reportes de calificaciones en función a al valor asignado a las actividades
                                        </p>

                                    </div>
                                </div>

                            </div>

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/medal.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Insignias y Competencias</a>
                                        </h4>
                                        <p>
                                            Crea sistemas para la generación de insignias y vincula tus actividades al logro de competencias educativas.
                                        </p>

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/benefits.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Gestión de Alumnos</a>
                                        </h4>
                                        <p>
                                            Administra el Alta y modificación de los perfiles de usuarios de tu colegio. Asígnalos a grupos y materias.
                                        </p>

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/backend.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Categorías</a>
                                        </h4>
                                        <p>
                                            Diseña Ciclos, niveles, grados y grupos escolares
                                        </p>

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/content.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Subcategorías</a>
                                        </h4>
                                        <p>
                                            Crea cursos, asignaturas o materias.
                                        </p>

                                    </div>
                                </div>

                            </div>

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/way.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Roles</a>
                                        </h4>
                                        <p>
                                            Administra los roles de profesores y alumnos y monitorea sus avances.
                                        </p>

                                    </div>
                                </div>

                            </div>

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/speak.svg" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Interacción</a>
                                        </h4>
                                        <p>
                                            Chats, Foros, Encuestas y Retroalimentación entre otras actividades.
                                        </p>

                                    </div>
                                </div>

                            </div>
                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap mt-20">

                                <div class="icons-item">
                                    <div class="item-box"> <img class="img-icon" src="public/images/Analytics-512.png" alt="" />
                                        <h4 class="icons-box-title">
                                            <a href="#">Analítica de aprendizaje</a>
                                        </h4>
                                        <p>
                                            Recopilación y análisis de datos sobre las interacciones de los alumnos en la plataforma Moodle para fortalecer el aprendizaje activo.
                                        </p>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>

            <!-- page-section -->
            <div class="holder-bg type-3 parallax-section" data-bg="public/images/75081.jpg" id="contact">

                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">

                            <h2 class="section-title" style="color: black;text-align: center;">Preguntas Frecuentes</h2>
                            <div class="accordion style-2">

                                <!--accordion item-->
                                <div class="accordion-item">
                                    <h6 class="a-title">¿Cuánto cuesta tener Moodle alojado?</h6>
                                    <div class="a-content">
                                        <p>
                                            Hay muchas opciones para alojamiento, SIA es una de ellas. El costo usualmente está relacionado con los servicios proporcionados
                                        </p>
                                        <p>
                                            y el número de usuarios de su colegio que usaran la plataforma en su colegio.
                                        </p>
                                    </div>
                                </div>

                                <!--accordion item-->
                                <div class="accordion-item">
                                    <h6 class="a-title active">¿Requiero de habilidades informáticas especiales para usar Moodle?</h6>
                                    <div class="a-content">
                                        <p>
                                            Sólo requiere habilidades básicas de navegación en internet. Para la instalación en un servidor si requiere
                                        </p>
                                        <p>
                                            cierto grado de conocimientos de informática o bien de consultoría especializada.
                                        </p>
                                    </div>
                                </div>

                                <!--accordion item-->
                                <div class="accordion-item">
                                    <h6 class="a-title">¿Puedo usar Moodle desde cualquier dispositivo o equipo de cómputo?</h6>
                                    <div class="a-content">
                                        <p>
                                            Moodle funciona bien en todos los sistemas operativos para equipos de escritorio y dispositivos móviles.
                                        </p>
                                        <p>
                                            El rendimiento puede variar dependiendo de las habilidades de sus alumnos, el ancho de banda que ellos empleen y los navegadores de internet que usen.
                                        </p>
                                    </div>
                                </div>

                                <!--accordion item-->
                                <div class="accordion-item">
                                    <h6 class="a-title">¿Qué tan seguro es Moodle?</h6>
                                    <div class="a-content">
                                        <p>
                                            Moodle está diseñado para ser muy seguro. Sin embargo, mucho depende del servidor web, la forma en la que está configurado Moodle y las actualizaciones regulares del paquete que haga el administrador de Moodle.
                                        </p>
                                        <p>
                                            SIA ofrece el Servicio de hosting de la plataforma en nuestros servidores respaldados con la tecnología Amazon Web Service.
                                        </p>
                                    </div>
                                </div>

                                <!--accordion item-->
                                <div class="accordion-item">
                                    <h6 class="a-title">¿Puede ser hackeado Moodle y que se roben los datos de los estudiantes?</h6>
                                    <div class="a-content">
                                        <p>
                                            Altamente improbable si su sitio Moodle tiene la seguridad actualizada y el administrador del sitio no ha proporcionado las claves.
                                        </p>
                                        <p>
                                            Todo es posible en un sitio web, pero Moodle hace difícil que los malvados causen daños.
                                        </p>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="col-sm-6">

                            <div class="title-holder">

                                <h2 class="section-title" style="color: black;text-align: center;">Cotiza</h2>
                                <!-- <p style="color: black;">Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat.</p> -->

                            </div>

                            <form style="text-align: center;">

                                <input style="border: 1px solid #001a40;" type="text" placeholder="Nombre/Organización">

                                <input style="border: 1px solid #001a40;" type="text" placeholder="Telefono">

                                <input style="border: 1px solid #001a40;" type="email" placeholder="E-mail">

                                <textarea style="border: 1px solid #001a40;" rows="5" placeholder="Mensaje"></textarea>

                                <a href="#" class="btn">Enviar</a>

                            </form>


                        </div>
                    </div>
                </div>
            </div>
@stop