<!doctype html>
<html lang="es">

<!-- Google Web Fonts
================================================== -->

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140671143-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-140671143-2');
</script>

<!-- Basic Page Needs
================================================== -->

<title>SIA es la empresa líder en consultoria y desarrollo de sistemas de administración escolar en México</title>

<meta charset="utf-8">
<meta name="author" content="ANIMATIOMX">
<meta name="keywords" content="sistema de administracion escolar, plataforma de control escolar, Moodle, software de administracion escolar, plataforma escolar, plataforma de gestión escolar">
<meta name="description" content="SIA proporciona consultoria y hosting  a escuelas, universidades y empresas para la instalación,configuración y personalización de la plataforma Moodle.">

<!-- Favicon -->
<link rel="shortcut icon" href="public/images/favicon.ico" />
<link rel="apple-touch-icon" href="public/images/favicon.ico" />

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Vendor CSS
============================================ -->

<link rel="stylesheet" href="public/font/demo-files/demo.css">
<link rel="stylesheet" href="public/plugins/revolution/css/settings.css">
<link rel="stylesheet" href="public/plugins/revolution/css/layers.css">
<link rel="stylesheet" href="public/plugins/revolution/css/navigation.css">

<!-- Vendor CSS
============================================ -->

<link rel="stylesheet" href="public/font/demo-files/demo.css">
<link rel="stylesheet" href="public/plugins/revolution/css/settings.css">
<link rel="stylesheet" href="public/plugins/revolution/css/layers.css">
<link rel="stylesheet" href="public/plugins/revolution/css/navigation.css">

<!-- CSS theme files
============================================ -->
<link rel="stylesheet" href="public/css/owl.carousel.css">
<link rel="stylesheet" href="public/css/bootstrap.min.css">
<link rel="stylesheet" href="public/css/fontello.css">
<link rel="stylesheet" href="public/css/styles.css">
<link rel="stylesheet" href="public/css/responsive.css">

</head>
<style>
    #footer[class*="footer"]:not(.footer-3):before {
        height: 0px !important;
    }
    #footer.footer-2:after {
        background: url(public/images/piepag-04.jpg) repeat-x !important;
    }
    #header.header-2 .pre-header:before {
        background: url(public/images/header_stripe1.png) repeat-x !important;
    }
</style>

<body>

    <div class="loader"></div>

    <!--cookie-->
    <!-- <div class="cookie">
		<div class="container">
		<div class="clearfix">
			<span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
			<div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
		</div>
		</div>
	</div>-->

    <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

    <div id="wrapper" class="wrapper-container">

        <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

        <nav id="mobile-advanced" class="mobile-advanced"></nav>

        <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

        <header id="header" class="header-2">

            <!-- search-form -->
            <div class="searchform-wrap">
                <div class="vc-child h-inherit relative">

                    <form>
                        <input type="text" name="search" placeholder="Start typing...">
                        <button type="button"></button>
                    </form>

                </div>
                <button class="close-search-form"></button>
            </div>

            <!-- pre-header -->
            <div class="pre-header">

                <div class="container">

                    <div class="row">
                        <div class="col-sm-6">

                            <div class="contact-info-menu">

                                <div class="contact-info-item">
                                    <i class="icon-mail-1"></i>
                                    <a href="mailto:contacto@siacademico.com">contacto@siacademico.com</a>
                                </div>
                                <!-- <div class="contact-info-item">
							<i class="icon-location"></i>
							<span>9870 St Vincent Place, Glasgow, DC 45 Fr 45</span>
						</div> -->
                            </div>
                        </div>
                        <div class="col-sm-6">

                            <div class="align-right">

                                <div class="contact-info-menu">
                                    <!-- <div class="contact-info-item lang-button">
								<i class="icon-globe-1"></i>
								<a href="#">English</a>
								<ul class="dropdown-list">
									<li><a href="#">English</a></li>
									<li><a href="#">German</a></li>
									<li><a href="#">Spanish</a></li>
								</ul>
							</div>
							<div class="contact-info-item">
								<i class="icon-globe-1"></i>
								<a href="#">Client/Register</a>
							</div> -->
                                    <!-- <div class="contact-info-item">
                                        <ul class="social-icons">
                                            <li class="fb-icon"><a href="#"><i class="icon-facebook"></i></a></li>
                                            <li class="google-icon"><a href="#"><i class="icon-gplus"></i></a></li>
                                            <li class="tweet-icon"><a href="#"><i class="icon-twitter"></i></a></li>
                                            <li class="insta-icon"><a href="#"><i class="icon-instagram-4"></i></a></li>

                                        </ul>
                                    </div> -->

                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>

            <!-- top-header -->
            <div class="top-header">

                <!-- - - - - - - - - - - - / Mobile Menu - - - - - - - - - - - - - -->

                <!--main menu-->

                <div class="menu-holder">

                    <div class="menu-wrap">

                        <div class="container">

                            <div class="table-row">

                                <!-- logo -->

                                <div class="logo-wrap">

                                    <a href="{{ url('/') }}" class="logo"><img src="public/images/logo.png" alt="" style="width: 40%;"></a>

                                </div>

                                <div class="call-us">
                                    <ul class="our-info-list">
                                        <li>
                                            <a href="tel:8132806645"><img src="public/images/phone.svg" style="width: 50px;" alt=""></a>
                                            <div>
                                                Llamanos:
                                                <a href="#">81 3280 6645</a>
                                            </div>

                                        </li>
                                    </ul>

                                    <ul class="our-info-list">
                                        <li>
                                            <a href="https://wa.me/528132806645?text=Me%20gustaría%20solicitar%20una%20cotización" target="_blank"><img src="public/images/whatsapp.svg" style="width: 50px;" alt=""></a>

                                            <div>
                                                Envia whatsapp:
                                                <a href="#">81 3280 6645</a>
                                            </div>

                                        </li>
                                    </ul>

                                    <ul class="our-info-list">
                                        <li>
                                            <a href="" target="_blank"><img src="public/images/telegram.png" style="width: 50px;" alt=""></a>

                                            <div>
                                                ANIMATIOMX
                                                <a href="#"></a>
                                            </div>

                                        </li>
                                    </ul>

                                    <ul class="our-info-list">
                                        <li>
                                            <a href="https://siacademico.com/plataforma/login/" target="_blank"><img src="public/images/login.svg" style="width: 30px;" alt=""></a>

                                        </li>
                                    </ul>
                                </div>

                            </div>

                        </div>

                        <!-- Menu & TopBar -->
                        <div class="nav-item">
                            <div class="container">
                                <nav id="main-navigation" class="main-navigation">
                                    <ul id="menu" class="clearfix">
                                        <li class="{{ request()->is('/') ? 'current' : '' }}"><a href="{{ url('/') }}">Colegios y Universidades</a>
                                        </li>
                                        <li class="{{ request()->is('Empresas') ? 'current' : '' }}"><a href="{{ url('Empresas') }}">Empresas</a>
                                        </li>
                                        <!-- <li class=""><a href="">SIA Pro</a>
                                        </li> -->
                                    </ul>
                                </nav>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- bottom-separator -->
            <div class="bottom-separator"></div>

        </header>

        <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
                <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

        <div id="content">

            <!-- <div style="position: fixed; top:50%;right:15px;z-index: 9999;">
                <div class="conta">
                    <a href="https://api.whatsapp.com/send?phone=522223591756" target="_blank">
                    <img class="icon-lu-zoom imgcnt" src="public/images/whatsapp.svg" alt="">
                    </a>
                </div>
                <div class="conta">
                    <a href="https://m.me/2034976176536614" target="_blank">
                    <img class="icon-lu-zoom imgcnt" src="public/images/messenger.svg" alt="">
                    </a>
                </div>
            </div> -->

        	@yield('content')
         </div>

        <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

        <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

        <footer id="footer" class="footer-2">

            <!-- Top footer -->
            <!-- <div class="top-footer">

                <div class="container">

                    <div class="row">


                        <div class="col-sm-3 col-xs-6">

                            <div class="widget">

                                <h5 class="widget-title">About Us</h5>
                                <p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.Vestibulum libero nisl.</p>

                                <ul class="social-icons style-2">

                                    <li class="fb-icon"><a href="#"><i class="icon-facebook"></i></a></li>
                                    <li class="google-icon"><a href="#"><i class="icon-gplus"></i></a></li>
                                    <li class="tweet-icon"><a href="#"><i class="icon-twitter"></i></a></li>
                                    <li class="insta-icon"><a href="#"><i class="icon-instagram-4"></i></a></li>

                                </ul>

                                <p class="copyright">Copyright <span>SuperOwl</span> © 2017. <br> All Rights Reserved</p>

                            </div>

                        </div>

                        <div class="col-sm-3 col-xs-6">

                            <div class="widget">
                                <h5 class="widget-title">Our Classes</h5>
                                <ul class="info-links">

                                    <li><a href="#">Music</a></li>
                                    <li><a href="#">Foreign Languages</a></li>
                                    <li><a href="#">Dance</a></li>
                                    <li><a href="#">Martial Arts</a></li>
                                    <li><a href="#">Drama</a></li>
                                    <li><a href="#">Sports Skills</a></li>
                                    <li><a href="#">Pre-reading</a></li>
                                    <li><a href="#">Pre-math</a></li>

                                </ul>
                            </div>

                        </div>

                        <div class="col-sm-3 col-xs-6">

                            <div class="widget">
                                <h5 class="widget-title">Quick Links</h5>
                                <ul class="info-links">

                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Our Team</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">Global Network</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">News</a></li>
                                    <li><a href="#">Contact Us</a></li>

                                </ul>
                            </div>

                        </div>

                        <div class="col-sm-3 col-xs-6">

                            <div class="widget">
                                <h5 class="widget-title">Latest News</h5>
                                <ul class="news-list">

                                    <li>

                                        <article class="entry">

                                            <div class="entry-body">


                                                <div class="entry-meta">

                                                    <time class="entry-date" datetime="2016-01-27">December 28, 2017</time>

                                                </div>



                                                <h6 class="entry-title"><a href="#">Vestibulum Sed Ante</a></h6>

                                            </div>


                                        </article>

                                    </li>
                                    <li>

                                        <article class="entry">

                                            <div class="entry-body">

                                                <div class="entry-meta">

                                                    <time class="entry-date" datetime="2016-01-21">December 21, 2017</time>

                                                </div>


                                                <h6 class="entry-title"><a href="#">Nam Elit Agna Endrerit Sit Amet</a></h6>

                                            </div>


                                        </article>

                                    </li>
                                    <li>

                                        <article class="entry">

                                            <div class="entry-body">


                                                <div class="entry-meta">

                                                    <time class="entry-date" datetime="2016-01-17">December 17, 2017</time>

                                                </div>



                                                <h6 class="entry-title"><a href="#">Donec Porta Diam Eu</a></h6>

                                            </div>

                                        </article>

                                    </li>

                                </ul>
                            </div>

                        </div>
                    </div>

                </div>

            </div> -->

        </footer>

        <div id="footer-scroll"></div>

        <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

    </div>

    <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

    <!-- JS Libs & Plugins
============================================ -->
    <script src="public/js/libs/jquery.modernizr.js"></script>
    <script src="public/js/libs/jquery-2.2.4.min.js"></script>
    <script src="public/js/libs/jquery-ui.min.js"></script>
    <script src="public/js/libs/retina.min.js"></script>
    <script src="public/plugins/jquery.scrollTo.min.js"></script>
    <script src="public/plugins/jquery.localScroll.min.js"></script>
    <script src="public/plugins/jquery.countdown.plugin.min.js"></script>
    <script src="public/plugins/jquery.countdown.min.js"></script>
    <script src="public/plugins/owl.carousel.min.js"></script>
    <script src="public/plugins/jquery.queryloader2.min.js"></script>
    <script src="public/plugins/revolution/js/jquery.themepunch.tools.min.js?ver=5.0"></script>
    <script src="public/plugins/revolution/js/jquery.themepunch.revolution.min.js?ver=5.0"></script>
    <script type="text/javascript" src="public/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="public/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="public/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>

    <!-- JS theme files
============================================ -->
    <script src="public/js/plugins.js"></script>
    <script src="public/js/script.js"></script>

</body>

</html>
